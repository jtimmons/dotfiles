;; style-gui.el
;; Contains style information for emacs running in the gui

(load-theme 'timely t)

;; Configure neo-tree to use icons nicely
(use-package all-the-icons :ensure t)
(setq neo-theme 'icons)

;; Clean up the UI
(scroll-bar-mode -1)
(tool-bar-mode -1)
(fringe-mode (cons 8 8))
(set-face-background 'fringe nil)
