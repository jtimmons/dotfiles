;; org-config.el

(org-babel-do-load-languages
 'org-babel-load-languages
 '((ditaa . t)))

(setq org-cycle-separator-lines 1)

(add-hook 'org-mode-hook
          (lambda ()
            (visual-line-mode t)))
