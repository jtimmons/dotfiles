;; general.el

(use-package transpose-frame :ensure t)  ;; Move frames
(use-package undo-tree :ensure t)        ;; Better undo logic
(use-package ace-jump-mode :ensure t)    ;; Supplemental movement
(use-package autopair :ensure t)         ;; Automatically pair quotes/parens
(use-package diminish :ensure t)         ;; Fewer modes in the toolbar
(use-package multiple-cursors :ensure t) ;; Multiple cursors
(use-package mwim :ensure t)             ;; Move Where I Mean
(use-package term :ensure t)             ;; For starting ansi-term
(use-package smooth-scroll :ensure t)    ;; For smoother-looking scrolling
(use-package editorconfig :ensure t)     ;; Editorconfig file support
(use-package neotree :ensure t)          ;; File browser
(use-package magit :ensure t)            ;; Better git integration
(use-package projectile :ensure t)       ;; Project management
(use-package dashboard :ensure t)        ;; Dashboard for startup

;; Fix ansi-term PATH on Mac OS
(when (memq window-system '(mac ns x))
  (use-package exec-path-from-shell :ensure t)
  (exec-path-from-shell-initialize))

;; Auto-exit ansi-term on process exit
(defadvice term-handle-exit
  (after term-kill-buffer-on-exit activate)
  (kill-buffer))

(autopair-global-mode 1)       ;; Automatically match pairs (quotes, parens, etc)
(global-undo-tree-mode 1)      ;; Undo-tree mode for better undoing
(projectile-mode +1)           ;; Project management

(add-hook 'term-mode-hook
	  (lambda ()
	    (autopair-mode -1)
	    (setq autopair-dont-activate t)))

;; Reduce mode-line clutter
(diminish 'autopair-mode)
(diminish 'undo-tree-mode)
(diminish 'abbrev-mode)

;; Generic stuff
(setq user-full-name "Justin Timmons")
(setq inhibit-startup-message t)       ;; Get rid of the startup message
(show-paren-mode t)                    ;; Always highlight parens
(setq scroll-step 1)                   ;; Scroll down one line at a time
(setq-default indent-tabs-mode nil)    ;; Spaces only
(setq-default tab-width 2)             ;; Default tab width
(setq-default c-basic-offset 2)        ;; Default C tab width
(setq python-indent 2)                 ;; Default Python tab width
(delete-selection-mode 1)              ;; Delete text when typing over a selection
(setq-default column-number-mode 1)    ;; Get those sweet, sweet column numbers
(setq web-mode-code-indent-offset 2)   ;; Fix web-mode indentation
(setq web-mode-markup-indent-offset 2) ;; Fix web-mode indentation
(smooth-scroll-mode 1)                 ;; Smooth scrolling with the mouse
(savehist-mode t)                      ;; Save minibuffer histories
(editorconfig-mode t)                  ;; Set settings based off of editorconfig
(setq neo-window-fixed-size nil)       ;; Allow resizing of neotree browser
(setq neo-autorefresh nil)             ;; Stop neotree from automatically updating
(setq projectile-dynamic-mode-line t)  ;; Show project type in the modeline
(setq projectile-switch-project-action 'my/projectile-switch-project-action)

;; Dashboard
(dashboard-setup-startup-hook)
(setq dashboard-center-content t)
(setq dashboard-startup-banner 'logo)
(setq dashboard-items '((projects . 10)
                        (recents  . 5)
                        (registers . 5)))
(setq dashboard-set-heading-icons t)
(setq dashboard-set-file-icons t)

;; Latex stuff
(setq TeX-parse-self t) ; Enable parse on load.
(setq TeX-auto-save t) ; Enable parse on save.

;; Put backup and auto-save files in /tmp
(setq backup-directory-alist `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;; Enable "advanced" commands
(put 'dired-find-alternate-file 'disabled nil)
(put 'downcase-region 'disabled nil)
