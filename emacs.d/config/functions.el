;; This file contains useful emacs functions

(use-package ffap :ensure t)


(defun find-at-point ()
  "Finds the current thing at this point. First attempts to find file, and then a tag"
  (interactive)
  (if (ffap-file-at-point)
      (find-file-at-point)
    (call-interactively 'find-tag)))

(defun delete-current-buffer-file ()
  "Removes file connected to current buffer and kills buffer."
  (interactive)
  (let ((filename (buffer-file-name))
	      (buffer (current-buffer))
	      (name (buffer-name)))
    (if (not (and filename (file-exists-p filename)))
	      (ido-kill-buffer)
      (when (yes-or-no-p "Are you sure you want to remove this file? ")
	      (delete-file filename)
	      (kill-buffer buffer)
	      (message "File '%s' successfully removed" filename)))))

(defun rename-current-buffer-file ()
  "Renames current buffer and the file it is visiting."
  (interactive)
  (let ((name (buffer-name))
	      (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
	      (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
	      (if (get-buffer new-name)
	          (error "A buffer named '%s' already exists!" new-name)
	        (rename-file filename new-name 1)
	        (rename-buffer new-name)
	        (set-visited-file-name new-name)
	        (set-buffer-modified-p nil)
	        (message "File '%s' successfully renamed to '%s'"
		               name (file-name-nondirectory new-name)))))))

(defun open-line-below ()
  "Opens a new line below the current one and moves to it"
  (interactive)
  (end-of-line)
  (newline)
  (indent-for-tab-command))

(defun move-line-down ()
  "Transposes with line below, keeping the current cursor position in line"
  (interactive)
  (let ((col (current-column)))
    (save-excursion
      (forward-line)
      (transpose-lines -1))
    (move-to-column col)))

(defun move-line-up ()
  "Transposes with line above, keeping the current cursor position in line"
  (interactive)
  (let ((col (current-column)))
    (save-excursion
      (forward-line)
      (transpose-lines -1))
    (move-to-column col)))

(if (not (fboundp 'string-prefix-p))
    (defun string-prefix-p (shorter longer)
      (let ((n (mismatch shorter longer))
            (l (length shorter)))
        (if (or (not n) (= n l)) l nil))))

(defun shell-command-maybe-region ()
  (interactive)
  (if (region-active-p) (call-interactively 'shell-command-on-region)
    (call-interactively 'shell-command)))

;; Keep region when undoing in region
(defadvice undo-tree-undo (around keep-region activate)
  (if (use-region-p)
      (let ((m (set-marker (make-marker) (mark)))
            (p (set-marker (make-marker) (point))))
        ad-do-it
        (goto-char p)
        (set-mark m)
        (set-marker p nil)
        (set-marker m nil))
    ad-do-it))

(defun new-ansi-term (&optional new-buffer-name)
  (interactive)
  (ansi-term "bash" new-buffer-name))

(defun goto-ansi-term ()
  (interactive)
  (let ((buffer-name "*ansi-term*"))
    (if (get-buffer buffer-name)
        (switch-to-buffer buffer-name)
      (new-ansi-term))))

(defun sudo ()
  "Opens current buffer as sudo user"
  (interactive)
  (let* ((file-name (buffer-file-name))
         (sudo-filename (concat "/sudo::" file-name))
         (position (point)))
    (find-alternate-file sudo-filename)
    (goto-char position)))

(defun rename-buffer-and-file-if-exists ()
  (interactive)
  (if (file-exists-p (buffer-file-name))
      (call-interactively 'rename-current-buffer-file)
    (call-interactively 'rename-buffer)))

(defun tail (file)
  (interactive "fFile Name: ")
  (progn
    (find-file-other-window file)
    (auto-revert-tail-mode)))

(defun mc/mark-lines-in-region ()
  (interactive)
  (when (region-active-p)
    (let ((num-lines (count-lines (region-beginning) (region-end))))
      (deactivate-mark)
      (goto-char (region-beginning))
      (mc/mark-next-lines (- num-lines 1)))))


(defun my/projectile-switch-project-action ()
  (ding)
  (projectile-find-file)
  (neotree-projectile-action))
