;;; timely-theme.el --- A dark theme with pastel colors that's easy on the eyes

;; Copyright (C) 2020 by Justin Timmons

;; Author: Justin Timmons <justinmtimmons@gmail.com>
;; Created: 24 Feb 2020
;; Version: 0.0.1
;; Keywords: faces
;; 

;; This file is not part of GNU Emacs.

;; This file is free software...
;; along with this file.  If not, see <https://www.gnu.org/licenses/>.

;; Code:

(deftheme timely
  "A dark theme with pastel colors that's easy on the eyes")

(let
    ;; Palette
    ((timely-white  "#ECECEC")
     (timely-black  "#2D3743")
     (timely-cyan   "#36FFFF")
     (timely-yellow "#FFFF80")
     (timely-purple "#FF80FF")
     (timely-green  "#80FF80")
     (timely-blue   "#8080FF")
     (timely-red    "#FF8080"))

  ;; Faces
  (custom-theme-set-faces
   'timely
   `(default ((t (:foreground ,timely-white :background ,timely-black))))

   ;; General fonts
   `(font-lock-keyword-face ((t (:foreground ,timely-cyan))))
   `(font-lock-variable-name-face ((t (:foreground ,timely-yellow))))
   `(font-lock-type-face ((t (:foreground ,timely-purple))))
   `(font-lock-string-face ((t (:foreground ,timely-green))))
   `(font-lock-constant-face ((t (:foreground ,timely-blue))))
   `(font-lock-function-name-face ((t (:foreground ,timely-blue))))
   `(font-lock-builtin-face ((t (:foreground ,timely-cyan))))
   `(font-lock-comment-face ((t (:foreground ,timely-red))))

   ;; Term colors
   `(term-color-black ((t (:background ,timely-black :foreground ,timely-black))))
   `(term-color-red ((t (:background ,timely-red :foreground ,timely-red))))
   `(term-color-green ((t (:background ,timely-green :foreground ,timely-green))))
   `(term-color-yellow ((t (:background ,timely-yellow :foreground ,timely-yellow))))
   `(term-color-blue ((t (:background ,timely-blue :foreground ,timely-blue))))
   `(term-color-magenta ((t (:background ,timely-purple :foreground ,timely-purple))))
   `(term-color-cyan ((t (:background ,timely-cyan :foreground ,timely-cyan))))
   `(term-color-white ((t (:background ,timely-white :foreground ,timely-white)))))

  ;; Variables
  (custom-theme-set-variables
   'timely

   ;; ansi-term fixes
   `(term-default-bg-color nil)
   `(term-default-fg-color nil)
   
   `(ansi-color-names-vector [,timely-black
                              ,timely-red
                              ,timely-green
                              ,timely-yellow
                              ,timely-blue
                              ,timely-purple
                              ,timely-cyan
                              ,timely-white])))

(provide-theme 'timely)
;;; timely-theme.el ends here
