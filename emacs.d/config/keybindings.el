;; keybindings.el
;; Adds custom keybindings to emacs

(require 'viper-cmd)  ;; Viper (vim fns) for better forward-words


(defun backward-delete-word (arg)
  (interactive "p")
  (delete-region (point) (progn (backward-word arg) (point))))

(global-set-key (kbd "C-z") nil) ;; Capture (and ignore) Ctrl-Z for suspending emacs

(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-x f") 'find-at-point)
(global-set-key (kbd "C-c C-SPC") 'ace-jump-mode)


;; NeoTree
(global-set-key [f8] 'neotree-toggle) ;; Open/close
(global-set-key (kbd "C-M-d") 'neotree-find)

;; Line shuffling
(global-set-key (kbd "ESC <C-up>") 'move-text-up)
(global-set-key (kbd "ESC <C-down>") 'move-text-down)
(global-set-key (kbd "M-RET") 'open-line-below)

;; Better file management
(global-set-key (kbd "C-x C-k") 'delete-current-buffer-file)
(global-set-key (kbd "C-x C-r") 'rename-buffer-and-file-if-exists)

;; Move cursor between windows
(global-set-key (kbd "<C-S-left>") 'windmove-left)
(global-set-key (kbd "<C-S-right>") 'windmove-right)
(global-set-key (kbd "<C-S-up>") 'windmove-up)
(global-set-key (kbd "<C-S-down>") 'windmove-down)

(define-key org-mode-map (kbd "<C-S-left>") 'windmove-left)
(define-key org-mode-map (kbd "<C-S-right>") 'windmove-right)
(define-key org-mode-map (kbd "<C-S-up>") 'windmove-up)
(define-key org-mode-map (kbd "<C-S-down>") 'windmove-down)

;; Better word management
(global-set-key (kbd "M-<left>") 'viper-backward-word)
(global-set-key (kbd "M-<right>") 'viper-forward-word)

(global-set-key (kbd "M-<backspace>") 'backward-delete-word)

(global-set-key (kbd "M-n") 'forward-paragraph)
(global-set-key (kbd "M-p") 'backward-paragraph)
(global-set-key (kbd "M-<down>") 'forward-paragraph)
(global-set-key (kbd "M-<up>") 'backward-paragraph)

(global-set-key (kbd "M-s") 'isearch-forward-regexp)          ;; Alt-search is regex
(global-set-key (kbd "M-S") 'isearch-forward-symbol-at-point) ;; Alt-capital searches symbol
(global-set-key (kbd "M-r") 'isearch-backward-regexp)         ;; Alt-search is regex
(global-set-key (kbd "M-%") 'query-replace-regexp)            ;; Default replace is regex
(global-set-key (kbd "M-#") 'comment-dwim)                    ;; Comment regions

(global-set-key (kbd "C-a") 'mwim-beginning-of-line-or-code)
(global-set-key (kbd "C-e") 'mwim-end-of-line-or-code)

(global-set-key (kbd "C-x C-c") 'save-buffers-kill-emacs)

;; Ansi-term windows
(global-set-key (kbd "C-M-n") 'new-ansi-term)
(define-key term-raw-map (kbd "C-M-n") 'new-ansi-term)
(define-key term-raw-map (kbd "C-M-d") 'neotree-find)
(define-key term-raw-map (kbd "M-x") 'execute-extended-command)
(define-key term-raw-map (kbd "C-c C-y") 'term-paste)                 ;; Hook into emacs kill-ring
(define-key term-raw-map (kbd "M-<backspace>") 'term-send-raw-meta)   ;; Fix delete-word
(define-key term-raw-map (kbd "C-<backspace>") 'term-send-raw-meta)   ;; Fix delete-word

(global-set-key [remap shell-command] 'shell-command-maybe-region)

(global-set-key (kbd "s-SPC") 'mc/mark-next-like-this)


;; Projectile
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
