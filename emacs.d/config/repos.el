;; repos.el
;; Loads repositories for emacs' package management system

(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa stable" . "http://stable.melpa.org/packages/"))

(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))


(setq package-archive-priorities '(("MELPA Stable" . 10)
                                   ("GNU ELPA"     . 5)
                                   ("MELPA"        . 0)))
