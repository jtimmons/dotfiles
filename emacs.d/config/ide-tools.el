;; ide-tools.el

;; Various File Formats
(use-package mermaid-mode :ensure t)
(use-package markdown-mode :ensure t)
(use-package yaml-mode :ensure t)
(use-package toml-mode :ensure t)
(use-package dockerfile-mode :ensure t)

(add-to-list 'markdown-code-lang-modes '(("mermaid" . mermaid-mode)))

;; WEB CONFIG
(use-package web-mode :ensure t)
(use-package flycheck :ensure t)
(use-package tide :ensure t)
(use-package company :ensure t)

;; Use web-mode for handling JS/JSX files
;; (add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.ts\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.dtl\\'" . web-mode))

;; Always use JSX rather than JS in web-mode
(add-hook 'web-mode-hook
          (lambda ()
            (if (equal web-mode-content-type "javascript")
                (web-mode-set-content-type "jsx")
              (message "now set to: %s" web-mode-content-type))))


;; Configure tide-mode
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode t)
  (setq flycheck-check-syntax-automatically '(save mode-enabled idle-change))
  (eldoc-mode t)
  (tide-hl-identifier-mode t)
  (company-mode t))

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

;; Run 'setup-tide-mode' when entering typescript modes
(add-hook 'typescript-mode-hook #'setup-tide-mode)
(add-hook 'web-mode-hook
          (lambda ()
            (when (or (string-equal "ts" (file-name-extension buffer-file-name))
                      (string-equal "tsx" (file-name-extension buffer-file-name))
                      (string-equal "js" (file-name-extension buffer-file-name))
                      (string-equal "jsx" (file-name-extension buffer-file-name)))
              (setup-tide-mode))))

;; Run flycheck checkers through npx
;; https://github.com/flycheck/flycheck/issues/1428
;; (with-eval-after-load 'flycheck
;;   (eval '(progn
;;            (push "npx" (flycheck-checker-get 'javascript-eslint 'command))
;;            (push "npx" (flycheck-checker-get 'typescript-tslint 'command)))))

;; Enable extra flycheck checkers
(flycheck-add-mode 'javascript-eslint 'web-mode)
(flycheck-add-next-checker 'javascript-eslint 'jsx-tide 'append)
(flycheck-add-next-checker 'javascript-eslint 'javascript-tide 'append)
