;;.emacs

;; Load other files
(add-to-list 'load-path "~/.emacs.d/config")
(add-to-list 'load-path "~/.emacs.d/ext")
(add-to-list 'custom-theme-load-path "~/.emacs.d/config/themes")

;; Provide packages built into higher versions of emacs for compatibility
(when (= emacs-major-version 23)
  (add-to-list 'load-path "~/.emacs.d/ext/compat/emacs23"))

;; Package manager and repositories
(require 'package)
(package-initialize)
(load "repos.el")

;; Ensure that use-package is installed
(when (not (require 'use-package nil t))
  (package-refresh-contents)
  (package-install 'use-package)
  (require 'use-package))

(load "functions.el")  ;; User-defined functions
(load "general.el")    ;; General configuration
(load "ide-tools.el")  ;; IDE-like features
(load "org-config.el") ;; Org-mode configuration
(load "layout.el")     ;; Windowing + layout configuration

;; Pretty colors
(if window-system
    (load "style-gui.el")
  (load "style-terminal.el"))

(load "keybindings.el")

;; "Customize" library output
(setq custom-file "~/.emacs.d/custom.el")
(when (file-exists-p custom-file)
  (load custom-file))
