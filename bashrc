# .bashrc
# For non-logon shells - use for environment variables and the like
# Will be executed each time a terminal is brought up

export PATH=~/.bin/:${PATH}    # add ~/.bin to path

# Don't do anything if we're in a non-interactive shell (SCP/subshells)
if [ -z "$PS1" ]; then
    return
fi

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

######################### START OF EXECUTION #########################
#                                                                    #
#   NOTE: Everything below this line is shell-agnostic and POSIX     #
#   compliant. It can be sources and run from any standard shell     #
#                                                                    #
######################################################################

export PATH=${PATH}:~/.local/bin  # add '--user' installed Python scripts to path
export HISTCONTROL=ignoredups  # do not record duplicates in bash history
umask u=rwx,g=rwx,o=rx         # better umask

alias pdb="python -m pdb"
alias mean="awk '{sum+=\$1} END {printf \"%02.9f\", sum/NR}'"
alias median="awk '{count++; values[count]=\$1} END {asort(values); if (count % 2) print values[(NR)-((NR-1)/2)]; else print (values[NR-((NR-1)/2)] + values[NR-(NR/2)+1])/2}'"
alias min="sort -n $1 | head -n 1"
alias max="sort -rn $1 | head -n 1"
alias stdev="awk '{sum+=\$1; sumsq+=\$1*\$1;} END {print sqrt(sumsq/NR - (sum/NR)**2)}'"
alias sum="awk '{sum+=\$1} END {print sum}'"
alias dif="awk 'p{print \$0-p}{p=\$0}'"
alias stats="awk '{count++; values[count]=\$1; sum+=\$1; sumsq+=\$1*\$1;} END {asort(values); print \"MIN:\",values[1]; print \"MAX\",values[NR]; print \"MEAN:\",sum/NR; if (count % 2) print \"MEDIAN:\",values[(NR)-((NR-1)/2)]; else print \"MEDIAN:\",(values[NR-((NR-1)/2)] + values[NR-(NR/2)+1])/2; print \"STDEV:\",sqrt(sumsq/NR - (sum/NR)**2); print \"NUM LINES:\",count;}'"

function get-ip() {
    ip route get 1 | awk '{print $NF; exit}'
}

man() {
    env \
	LESS_TERMCAP_mb=$(printf "\e[1;31m") \
	LESS_TERMCAP_md=$(printf "\e[1;31m") \
	LESS_TERMCAP_me=$(printf "\e[0m") \
	LESS_TERMCAP_se=$(printf "\e[0m") \
	LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
	LESS_TERMCAP_ue=$(printf "\e[0m") \
	LESS_TERMCAP_us=$(printf "\e[1;32m") \
	man "$@"
}
